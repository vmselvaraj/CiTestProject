﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SqlDatabaseBrowser
{
    /// <summary>
    /// Interaction logic for TableViewersContainer.xaml
    /// </summary>
    public partial class TableViewersContainer : UserControl
    {
        private static Action EmptyDelegate = delegate() { };
        public Settings Settings { get; set; }

        public TableViewersContainer()
        {
            InitializeComponent();
        }

        public List<TableViewer> TableViewers = null; 
        public DatabaseInfo DbInfo { get; set; }

        public TableViewersContainer(DatabaseInfo dbInfo) : this()
        {
            DbInfo = dbInfo;
            TableViewers = new List<TableViewer>();
            //this.Loaded += TableViewersContainer_Loaded;
        }

        void TableViewersContainer_Loaded(object sender, RoutedEventArgs e)
        {
            //InitializeFirstControl();
        }

        private void InitializeFirstControl()
        {
            AddViewerControl();
        }

        private void addViewer_Click(object sender, RoutedEventArgs e)
        {
            AddViewerControl();
        }

        public TableViewer AddViewerControl()
        {
            return AddViewerControl(string.Empty, string.Empty, string.Empty);
        }

        public TableViewer AddViewerControl(string tableName, string mapKeyForNext, string mapKeyForPrevious)
        {
            bool isFirstControl = false;
            if(tableViewerContainer.Children.Count == 1)
                isFirstControl = true;

            var viewerControl = new TableViewer(DbInfo, tableName, mapKeyForNext, mapKeyForPrevious, isFirstControl);
            AddToContainer(viewerControl);
            return viewerControl;
        }

        private void AddToContainer(TableViewer viewer)
        {
            viewer.SelectionChanged += viewer_SelectionChanged;
            viewer.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            viewer.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            viewer.VerticalContentAlignment = System.Windows.VerticalAlignment.Stretch;
            viewer.Margin = new Thickness(10);
            TableViewers.Add(viewer);
            DockPanel.SetDock(viewer, Dock.Left);
            tableViewerContainer.Children.Insert(tableViewerContainer.Children.Count - 1, viewer);
        }

        void viewer_SelectionChanged(TableViewer viewer, string value)
        {
            int selectionChangedViewedIndex = TableViewers.IndexOf(viewer);

            if(selectionChangedViewedIndex != TableViewers.Count -1)
            {
                var nextViewer = TableViewers[selectionChangedViewedIndex + 1];
                nextViewer.SendPreviousTableMapKeyValue(value);
            }
        }

        private void saveView_Click(object sender, RoutedEventArgs e)
        {
            View newView = new View(this.DbInfo.Database, TableViewers);
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Sql Browser View File | *.sbf";
            if(saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                View.Save(newView, fileName);

                Settings.AddRecentView(new ViewInfo(fileName));
                Settings.Persist();
            }
        }
    }
}
