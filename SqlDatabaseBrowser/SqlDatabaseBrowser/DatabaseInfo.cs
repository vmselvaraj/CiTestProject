﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace SqlDatabaseBrowser
{
    public class DatabaseInfo
    {
        private string m_ConnectionString = string.Empty;
        private List<string> m_ListOfTables = null;

        public Database Database { get; set; }

        public DatabaseInfo(Database database) : this(database.ConnectionString)
        {
            Database = database;
        }

        public string ConnectionString
        {
            get { return m_ConnectionString; }
            set { m_ConnectionString = value; }
        }

        public List<string> ListOfTables
        {
            get { return m_ListOfTables; }
            set { m_ListOfTables = value; }
        }

        public DatabaseInfo(string connectionString)
        {
             ConnectionString = connectionString;
        }

        public bool Prepare()
        {
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(ConnectionString);
                connection.Open();

                ListOfTables = new List<string>();
                DataTable schema = connection.GetSchema("Tables");
                List<string> TableNames = new List<string>();
                foreach (DataRow row in schema.Rows)
                {
                    ListOfTables.Add(row[2].ToString());
                }
            }
            catch
            {
                return false;
            }
            finally
            {
                if(connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                    connection = null;
                }
            }

            ListOfTables.Sort();

            return true;
        }
    }
}
