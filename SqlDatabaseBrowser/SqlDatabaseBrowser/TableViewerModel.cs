﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlDatabaseBrowser
{
    public class TableViewerModel
    {
        public string TableName { get; set; }
        public Key Keys { get; set; }
    }
}
