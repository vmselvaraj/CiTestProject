﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SqlDatabaseBrowser
{
    [Serializable]
    public class View
    {
        public Database Database { get; set; }
        public List<ViewItem> ViewItems { get; set; }

        public View()
        {

        }
        
        public View(Database database, List<TableViewer> tableViewers)
        {
            Database = database;
            ViewItems = new List<ViewItem>();
            foreach(TableViewer view in tableViewers)
            {
                var newViewItem = new ViewItem();
                newViewItem.TableName = view.ViewModel.TableName;
                newViewItem.MapKeyForNext = view.ViewModel.MapKeyForNext;
                newViewItem.MapKeyForPrevious = view.ViewModel.MapKeyForPrevious;
                ViewItems.Add(newViewItem);
            }
        }

        public static void Save(View view, string filePath)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(View));
            StreamWriter writer = new StreamWriter(filePath);
            serializer.Serialize(writer, view);
            writer.Close();
            writer.Dispose();
        }

        public static View Load(string filePath)
        {
            View view = null;
            XmlSerializer deserializer = new XmlSerializer(typeof(View));
            StreamReader reader = new StreamReader(filePath);
            view = deserializer.Deserialize(reader) as View;
            return view;
        }
    }


    [Serializable]
    public class ViewItem
    {
        public string TableName { get; set; }
        public string MapKeyForPrevious { get; set; }
        public string MapKeyForNext { get; set; }
    }
}
