﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
namespace SqlDatabaseBrowser
{
    [Serializable]
    public class Settings
    {
        private int numberOfRecentDatabases = 5;
        private int numberOfRecentViews = 10;

        public List<Database> RecentDatabases { get; set; }
        public List<ViewInfo> RecentViews { get; set; }

        public Settings()
        {
            RecentDatabases = new List<Database>();
            RecentViews = new List<ViewInfo>();
        }

        public void AddRecentDatabase(Database database)
        {
            Database existingDatabase = RecentDatabases.Find(x => x.ConnectionString == database.ConnectionString);
            if(existingDatabase != null)
                RecentDatabases.Remove(existingDatabase);
            else if (RecentDatabases.Count > numberOfRecentDatabases)
                RecentDatabases.RemoveAt(numberOfRecentDatabases - 1);

            RecentDatabases.Insert(0, database);
        }

        public void AddRecentView(ViewInfo viewInfo)
        {
            ViewInfo existingViewInfo = RecentViews.Find(x => x.Name == viewInfo.Name && x.FilePath == viewInfo.FilePath);
            if (existingViewInfo != null)
                RecentViews.Remove(existingViewInfo);
            else if (RecentViews.Count > numberOfRecentViews)
                RecentViews.RemoveAt(numberOfRecentDatabases - 1);
            RecentViews.Insert(0, viewInfo);
        }

        public void Persist()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Settings));
            using (StreamWriter writer = new StreamWriter(Paths.SettingsFilePath, false))
            {
                serializer.Serialize(writer, this);
            }
        }

        public static Settings Load()
        {
            Settings settings = null;
            XmlSerializer serializer = new XmlSerializer(typeof(Settings));

            if (File.Exists(Paths.SettingsFilePath))
            {
                using (StreamReader reader = new StreamReader(Paths.SettingsFilePath))
                {
                    settings = serializer.Deserialize(reader) as Settings;
                }
            }

            if (settings == null)
                settings = new Settings();

            return settings;
        }
    }
}
