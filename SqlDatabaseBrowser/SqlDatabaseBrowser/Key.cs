﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlDatabaseBrowser
{
    public class Key
    {
        public string PreviousTableMap { get; set; }
        public string NextTableMap { get; set; }
        
    }
}
