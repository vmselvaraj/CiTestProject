﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SqlDatabaseBrowser
{
    public static class Paths
    {
        
        private static string UserAppDataPath
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString();
            }
        }

        public static string SettingsFilePath
        {
            get
            {
                return Path.Combine(UserAppDataPath, "SqlDatabaseBrowser.xml");
            }
        }
    }
}
