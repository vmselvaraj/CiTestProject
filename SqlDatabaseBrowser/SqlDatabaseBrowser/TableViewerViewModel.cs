﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlDatabaseBrowser
{
    public class TableViewerViewModel : INotifyPropertyChanged
    {
        public  DatabaseInfo DbInfo { get; set; }
        public bool IsFirstTable { get; set; }

        public bool ShowPreviousTableMap
        {
            get
            {
                return !IsFirstTable;
            }
        }

        public TableViewerViewModel(DatabaseInfo dbInfo, bool isFirstTable)
        {
            DbInfo = dbInfo;
            IsFirstTable = isFirstTable;
            TableColumns = new ObservableCollection<string>();
            Data = new DataTable();
        }

        public TableViewerViewModel(DatabaseInfo dbInfo, bool isFirstTable, string tableName, string mapKeyForPrevious, string mapKeyForNext) : this(dbInfo, isFirstTable)
        {
            TableName = tableName;
            MapKeyForPrevious = mapKeyForPrevious;
            MapKeyForNext = mapKeyForNext;
        }

        private DataTable AllData { get; set; }

        private DataTable m_Data = null;

        public DataTable Data
        {
            get { return m_Data; }
            set { m_Data = value; }
        }

        public DataView DataView
        {
            get
            {
                return AllData.DefaultView;
            }
        }

        private string m_SelectedKeyValue = string.Empty;
        public string SelectedKeyValue
        {
            get
            {
                return m_SelectedKeyValue;
            }
            set
            {
                m_SelectedKeyValue = value;
                OnPropertyChanged("SelectedKeyValue");
            }
        }

        private ObservableCollection<string> m_TableColumns = null;
        public ObservableCollection<string> TableColumns
        {
            get
            {
                return m_TableColumns;
            }
            set
            {
                m_TableColumns = value;
                OnPropertyChanged("TableColumns");
            }
        }

        private string m_TableName = string.Empty;
        public string TableName
        {
            get
            {
                return m_TableName;
            }
            set
            {
                if (value != m_TableName)
                {
                    m_TableName = value;
                    OnPropertyChanged("TableName");
                    PopulateTableColumns();
                    PopulateTableData();
                    RefreshSelection(string.Empty);
                }
            }
        }

        #region Map Keys

        private string m_MapKeyForNext = string.Empty;
        public string MapKeyForNext
        {
            get
            {
                return m_MapKeyForNext;
            }
            set
            {
                if (value != m_MapKeyForNext)
                {
                    m_MapKeyForNext = value;
                    OnPropertyChanged("MapKeyForNext");
                }
            }
        }


        private string m_MapKeyForPrevious = string.Empty;
        public string MapKeyForPrevious
        {
            get
            {
                return m_MapKeyForPrevious;
            }
            set
            {
                if (value != m_MapKeyForPrevious)
                {
                    m_MapKeyForPrevious = value;
                    OnPropertyChanged("MapKeyForPrevious");
                }
            }
        }

        #endregion 

        public string RefreshSelection(string mapKeyValue)
        {
            if (!string.IsNullOrEmpty(TableName))
            {
                if (string.IsNullOrEmpty(MapKeyForPrevious))
                    AllData.DefaultView.RowFilter = string.Empty;   //show all the rows
                else if (string.IsNullOrEmpty(mapKeyValue))
                    if (TableColumns.Count > 0)
                        AllData.DefaultView.RowFilter = string.Format("0 = 1");//Show no rows
                    else
                        AllData.DefaultView.RowFilter = string.Empty;  //Show all rows anyways it is going to be empty
                else
                    AllData.DefaultView.RowFilter = string.Format("{0} = '{1}'", MapKeyForPrevious, mapKeyValue); //show matched rows
            }

            OnPropertyChanged("DataView");
            return SelectedKeyValue;
        }

        private void PopulateTableColumns()
        {
            TableColumns.Clear();

            PopulateTableColumnsHelper(string.Format("select c.name from sys.columns c inner join sys.tables t on t.object_id = c.object_id and t.name = '{0}' and t.type = 'U'", TableName));
            //If Table Columns count is Zero, then it could a View 
            if(TableColumns.Count == 0)
            {
                PopulateTableColumnsHelper(string.Format("select c.name from sys.columns c inner join sys.views t on t.object_id = c.object_id and t.name = '{0}' and t.type = 'V'", TableName));
            }
        }

        private void PopulateTableColumnsHelper(string commandText)
        {
            using (SqlConnection connection = new SqlConnection(DbInfo.ConnectionString))
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = commandText;
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TableColumns.Add(reader.GetString(0));
                        }
                    }
                    connection.Close();
                }
            }
        }

        private void PopulateTableData()
        {
            using (SqlConnection connection = new SqlConnection(DbInfo.ConnectionString))
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = string.Format("select * from {0}", TableName);
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);

                    connection.Close();
                    AllData = ds.Tables[0];
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
