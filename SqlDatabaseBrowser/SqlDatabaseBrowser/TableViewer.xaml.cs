﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SqlDatabaseBrowser
{
    /// <summary>
    /// Interaction logic for TableViewer.xaml
    /// </summary>
    public partial class TableViewer : UserControl
    {
        public event SelectionChangedDelegate SelectionChanged = null;
        private TableViewerViewModel vm = null;
        public TableViewerViewModel ViewModel
        {
            get
            {
                return vm;

            }
            set
            {
                vm = value;
                this.DataContext = value;
            }
        }
        public TableViewer()
        {
            InitializeComponent();
        }

        public TableViewer(DatabaseInfo dbInfo, bool isFirstTable) : this()
        {
            ViewModel = new TableViewerViewModel(dbInfo, isFirstTable);
        }

        public TableViewer(DatabaseInfo dbInfo, string tableName, string mapKeyForNext, string mapKeyForPrevious, bool isFirstTable)
            : this(dbInfo, isFirstTable)
        {
            ViewModel.TableName = tableName;
            ViewModel.MapKeyForNext = mapKeyForNext;
            ViewModel.MapKeyForPrevious = mapKeyForPrevious;
        }

        private void tableDetails_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string value = string.Empty;
            if (tableDetails.SelectedItem != null)
            {
                DataRowView rowView = tableDetails.SelectedItem as DataRowView;
                if (!string.IsNullOrEmpty(vm.MapKeyForNext))
                {
                    value = rowView.Row[ViewModel.MapKeyForNext].ToString();
                }
            }

            if (SelectionChanged != null)
                SelectionChanged(this, value);
        }

        public void SendPreviousTableMapKeyValue(string value)
        {
            ViewModel.RefreshSelection(value);

            if (tableDetails.Items.Count != 0)
                tableDetails.SelectedItem = tableDetails.Items[0];
            else
                if (SelectionChanged != null)
                    SelectionChanged(this, null);

        }

        private void tableDetails_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Contains('.') && e.Column is DataGridBoundColumn)
            {
                DataGridBoundColumn dataGridBoundColumn = e.Column as DataGridBoundColumn;
                dataGridBoundColumn.Binding = new Binding("[" + e.PropertyName + "]");
            }
        }
    }
}
