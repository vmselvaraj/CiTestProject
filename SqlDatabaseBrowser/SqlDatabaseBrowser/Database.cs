﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlDatabaseBrowser
{
    [Serializable]
    public class Database
    {
        public string DataSource { get; set; }
        public string InitialCatalog {get;set;}
        public string ConnectionString { get; set; }
        public string DisplayString
        {
            get
            {
                return string.Format("{0} ,({1})", InitialCatalog, DataSource);
            }
        }
    }
}
