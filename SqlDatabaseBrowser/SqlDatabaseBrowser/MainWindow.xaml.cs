﻿using Microsoft.Data.ConnectionUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace SqlDatabaseBrowser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MahApps.Metro.Controls.MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }

        public Settings Settings
        {
            get
            {
                return this.DataContext as Settings;
            }
            set
            {
                this.DataContext = value;
            }
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
             Settings =  Settings.Load();
        }

        private void connecToDatabase_Click(object sender, RoutedEventArgs e)
        {
            DataConnectionDialog dlg = new DataConnectionDialog();
            DataSource.AddStandardDataSources(dlg);
            if (DataConnectionDialog.Show(dlg) == System.Windows.Forms.DialogResult.OK)
            {
                SqlConnectionStringBuilder connectionBuilder = new SqlConnectionStringBuilder(dlg.ConnectionString);
                Database newDatabaseConnect = new Database
                {
                    ConnectionString = dlg.ConnectionString,
                    DataSource = connectionBuilder.DataSource,
                    InitialCatalog = connectionBuilder.InitialCatalog
                };

                InitializeContainer(newDatabaseConnect);
            }
            dlg.Dispose();
        }

        private TableViewersContainer InitializeContainer(Database database)
        {
            TableViewersContainer container = null;
            if (database != null)
            {
                DatabaseInfo dbInfo = new DatabaseInfo(database);
                dbInfo.Prepare();

                container = new TableViewersContainer(dbInfo);
                Grid.SetColumnSpan(container, 2);
                container.Settings = this.Settings;
                container.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                container.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;

                root.Children.Add(container);

                connecToDatabase.Visibility = System.Windows.Visibility.Collapsed;
                recents.Visibility = System.Windows.Visibility.Collapsed;

                Settings.AddRecentDatabase(database);
                Settings.Persist();
            }
            return container;
        }

        private void recentDatabases_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InitializeContainer(recentDatabases.SelectedItem as Database);
        }

        private void recentViews_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewInfo selectedInfo = recentViews.SelectedItem as ViewInfo;
            if(selectedInfo != null && File.Exists(selectedInfo.FilePath))
            {
                View v = View.Load(selectedInfo.FilePath);
                var container  = InitializeContainer(v.Database);
                if(container != null)
                {
                    foreach(var viewItem in v.ViewItems)
                    {
                        container.AddViewerControl(viewItem.TableName, viewItem.MapKeyForNext, viewItem.MapKeyForPrevious);
                    }
                }
            }

            Settings.AddRecentView(selectedInfo);
            Settings.Persist();
        }
    }
}
