﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlDatabaseBrowser
{
    [Serializable]
    public class ViewInfo
    {
        public string Name { get; set; }
        public string FilePath { get; set; }

        public ViewInfo()
        {

        }

        public ViewInfo(string viewFileFullPath)
        {
            Name = Path.GetFileName(viewFileFullPath);
            FilePath = viewFileFullPath;
        }
    }
}
